package uk.me.krupa.kruton.gateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.cloud.gateway.route.builder.filters
import org.springframework.cloud.gateway.route.builder.routes
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono
import java.net.URI

@SpringBootApplication
class KrutonGatewayApplication {

    @Bean
    fun fallbackRoutes() = router {
        println("ROUTER!")
        accept(MediaType.APPLICATION_JSON).nest {
            GET("/failure") { req -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(Mono.just("{}"), String::class.java) }
        }
    }

    @Bean
    fun routes(builder: RouteLocatorBuilder): RouteLocator = builder.routes {

        route("dictionary") {
            path("/dictionary/**")
            filters {
                stripPrefix(1)
                hystrix {
                    it.name = "Blah"
                    it.fallbackUri = URI("forward:/failure")
                }
            }
            uri("http://kruton-dictionary-service")
        }

        route("alias") {
            path("/alias/**")
            filters { stripPrefix(1) }
            uri("http://kruton-alias-service")
        }
    }
}

fun main(args: Array<String>) {
    runApplication<KrutonGatewayApplication>(*args)
}
